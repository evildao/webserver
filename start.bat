@echo off
color 87
title Nginx^+PHP 启动器

rem ---------关闭相关服务------------------------
echo Stopping nginx...
taskkill /F /IM nginx.exe > nul

echo Stopping PHP FastCGI...
taskkill /F /IM php-cgi-spawner.exe > nul
taskkill /F /IM php-cgi.exe > nul

rem ----------启动相关服务-----------------------
echo Starting PHP FastCGI...
D:\WMNP\RunHiddenConsole.exe D:\WMNP\php-cgi-spawner.exe "D:\WMNP\P\php-5.5.33-nts\php-cgi.exe -c D:\WMNP\P\php-5.5.33-nts\php.ini" 9055 4
D:\WMNP\RunHiddenConsole.exe D:\WMNP\php-cgi-spawner.exe "D:\WMNP\P\7.2.7-nts-x64\php-cgi.exe -c D:\WMNP\P\7.2.7-nts-x64\php.ini" 9072 4

echo Starting Nginx...
D:\WMNP\RunHiddenConsole.exe D:\WMNP\N\nginx.exe -p D:\WMNP\N

rem ---------- 延迟关闭 -----------------------
echo Successful!
choice /t 3 /d y /n >nul